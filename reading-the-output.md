# Breakdown of generated .xlsx file

Evaluate generates an xlsx file containing the following worksheets:

- App Stats: General statistics about the instance
- Evaluate Report: A summarized description of the content from the flagged projects worksheet followed by next steps to increase the likelihood of successfully migrating the flagged project
- Flagged Projects: A subset of the raw project data containing only projects that were flagged as being potentially problematic to migrate
- Users: A list of all users on the instance, their state (active or blocked), and if they take up a license seat
- Raw Project Data: All project data we retrieve while running Evaluate

# Reading the Flagged Projects and Raw Project Data

| Component | Impact | Mitigation (pre-migration) |
|-|-|-|
| pipelines | While artifacts and logs are not exported, other information about the pipeline is (related branch refs, etc). Each entry is a line of `ndjson` in the export (size) that must then be imported to the destination system (time impact on database) | Delete pipelines from source [via API](https://docs.gitlab.com/ee/api/pipelines.html#delete-a-pipeline) |
| issues | Each entry is a line of `ndjson` in the export (size) that must then be imported to the destination system (time impact on database) | Delete issues from the source [via API](https://docs.gitlab.com/ee/api/issues.html#delete-an-issue).<br><br>**NOTE:** Some customer are unwilling to lose this history |
| merge requests | Each entry is a line of `ndjson` in the export (size) that must then be imported to the destination system (time impact on database) | Delete merge requests from the source [via API](https://docs.gitlab.com/ee/api/merge_requests.html#delete-a-merge-request).<br><br>**NOTE:** Some customer are unwilling to lose this history |
| repository | The [usage quota](https://docs.gitlab.com/ee/user/usage_quotas.html#view-storage-usage) listed for a project repository may not represent the size of the complete `project.bundle` that will actually be exported. We have seen in practice 150 MB of listed usage turn into a 2.6 GB `project.bundle` in the tar file. This can drive the file size up past our 5 GB (Cloudflare) or 10 GB (S3 file import) numbers. Further, there is a database component to the imports with Gitaly Cluster that should be considered. | Use something like [git filter-repo](https://github.com/newren/git-filter-repo) or [BFG](https://rtyley.github.io/bfg-repo-cleaner/) to trim the repository |
| uploads | Uploads are the items (images, binaries, etc) attached to issues, merge requests, commits, etc. They are exported as part of the tar file and can take significant space, pushing up the size of the export | **TODO:** Check if using `filter-repo` to shrink the repository will also remove uploads. |
| LFS | LFS items are items checked directly into the repository on systems where `git LFS` is enabled. They are exported as part of the tar file and can take significant space, pushing up the size of the export. | **TODO:** Check if using `filter-repo` to shrink the repository will also remove LFS items attached to pruned branches. |
| Wiki | The wiki is exported as a separate repository bundle called `project.wiki.bundle` in the tar file | Delete/prune Wiki pages as needed [via API](https://docs.gitlab.com/ee/api/wikis.html#delete-a-wiki-page). |
| Snippets | Snippets are exported as individual bundles | Delete/prune Snippets as needed [via API](https://docs.gitlab.com/ee/api/project_snippets.html#delete-snippet). |
| branches | See "repository" component. | |
| commits | See "repository" component. | |
| job artifacts and trace | As job (pipeline) artifacts and the trace logs are not exported, this does not affect migrations. | There is [an open `congregate` issue](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate/-/issues/656) to pull and archive this data, but not migrate it. |
| tags | See "repository" component. | |
| package types | Congregate currently only supports: Docker/OCI Containers and Maven. Also, the sheer `number` and `size` of the registries can slow migrations, and impact storage on GitLab.com. Whenever possible, customer should delete/expire old images, or simply ignore the old images and regenerate the needed images and image tags on the destination system. | <ul><li>[Delete container tags in bulk](https://docs.gitlab.com/ee/api/container_registry.html#delete-registry-repository-tags-in-bulk)</li><li>[Delete project packages](https://docs.gitlab.com/ee/api/packages.html#delete-a-project-package)</li><li>[Delete project package files](https://docs.gitlab.com/ee/api/packages.html#delete-a-package-file) </li></ul> |
| container registry size | See "package types" component. | |
